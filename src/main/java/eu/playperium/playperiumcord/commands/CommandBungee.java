package eu.playperium.playperiumcord.commands;

import eu.playperium.playperiumcord.server.PlayPeriumServer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class CommandBungee extends Command {

    public CommandBungee(String name) {
        super("bungee", null);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        sender.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "PlayPeriumCord");
        sender.sendMessage(ChatColor.GRAY + "   Version: " + PlayPeriumServer.getPluginVersion());
        sender.sendMessage(ChatColor.GRAY + "   Minecraft: " + PlayPeriumServer.getMinecraftVersion());
        sender.sendMessage(ChatColor.GRAY + "A fork of BungeeCord by md_5");
    }
}
