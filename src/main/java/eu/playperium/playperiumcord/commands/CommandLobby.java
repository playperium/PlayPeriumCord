package eu.playperium.playperiumcord.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandLobby extends Command {

    public CommandLobby(String name) {
        super("lobby", null);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        if(sender instanceof ProxiedPlayer){
            ProxiedPlayer player = (ProxiedPlayer) sender;

            // ToDo: Get Lobby Server from .json Config File and get the Servers wit a methode to replace the getServerInfo.
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo("lobby");

            player.connect(serverInfo);
        }
    }
}
