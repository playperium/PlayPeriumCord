package eu.playperium.playperiumcord.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class CommandHelp extends Command {

    public CommandHelp(String name) {
        super("help", null);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        sender.sendMessage(ChatColor.GOLD + "Für Hilfe besuche unser Wiki: wiki.playperium.eu");
    }
}
