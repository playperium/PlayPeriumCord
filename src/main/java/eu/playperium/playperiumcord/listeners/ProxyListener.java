package eu.playperium.playperiumcord.listeners;

import eu.playperium.playperiumcord.protocol.Protocol;
import eu.playperium.playperiumcord.server.PlayPeriumServer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class ProxyListener implements Listener {

    private eu.playperium.playperiumcord.PlayPeriumCord plugin;

    public ProxyListener(eu.playperium.playperiumcord.PlayPeriumCord main) {
        this.plugin = main;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onProxyPing(ProxyPingEvent e) {
        ServerPing ping = e.getResponse();

        if (Protocol.supportedVersions.contains(e.getConnection().getVersion())) {
            ping.setVersion(new ServerPing.Protocol("PlayPeriumCord " + PlayPeriumServer.getPluginVersion(), e.getConnection().getVersion()));
        } else {
            ping.setVersion(new ServerPing.Protocol("Unsupported!", 5));
            ping.setDescription(ChatColor.RED + "Es ist kein Server für deine Minecraft Version verfügbar! Bitte versuche es mit einer anderen.");
        }
    }
}
