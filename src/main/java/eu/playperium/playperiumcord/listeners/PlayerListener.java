package eu.playperium.playperiumcord.listeners;

import eu.playperium.playperiumcord.server.ServerKickReasons;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListener implements Listener {

    private eu.playperium.playperiumcord.PlayPeriumCord plugin;

    public PlayerListener(eu.playperium.playperiumcord.PlayPeriumCord main) {
        this.plugin = main;
    }

    @EventHandler
    public void onServerKick(ServerKickEvent e) {
        String kickReason = BaseComponent.toPlainText(e.getKickReasonComponent());

        if (kickReason.contains(ServerKickReasons.FORGE_MOD_REJECTIONS)) {
            e.setKickReasonComponent(TextComponent.fromLegacyText(ChatColor.RED + "Du scheinst nicht die richtige Modpack-Version zu verwenden, bitte passe diese an, um auf diesem Server spielen zu können! Für weitere Hilfe besuche die Seite: wiki.playperium.eu"));
        } else {
            e.setCancelled(true);
        }
    }
}
