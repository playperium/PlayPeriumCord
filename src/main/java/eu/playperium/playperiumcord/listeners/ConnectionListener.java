package eu.playperium.playperiumcord.listeners;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ConnectionListener implements Listener {

    private eu.playperium.playperiumcord.PlayPeriumCord plugin;

    public ConnectionListener(eu.playperium.playperiumcord.PlayPeriumCord main) {
        this.plugin = main;
    }

    @EventHandler
    public void onServerConnect (ServerConnectEvent e) {
        ProxiedPlayer player = e.getPlayer();
        ServerInfo serverInfo = e.getTarget();

        // ToDo: Get Lobby Server from .json Config File and get the Servers wit a methode to replace the String.
        if (serverInfo.getName().equals("lobby")) {
            player.sendMessage(ChatColor.GOLD + "Du wirst zu einer Lobby verbunden...");
        } else {
            player.sendMessage(ChatColor.GOLD + "Du wirst zum Server verbunden...");
        }
    }

    @EventHandler
    public void onServerConnected (ServerConnectedEvent e) {
        ProxiedPlayer player = e.getPlayer();

        // ToDo: Get Lobby Server from .json Config File and get the Servers wit a methode to replace the String.
        /*if (player.isForgeUser() && e.getServer().getInfo().getName().equals("lobby")) {
            player.sendMessage(ChatColor.RED + "Achtung: Das Verbinden zu einen Modpack-Server kann einen Moment dauern!");
        }*/
    }
}
