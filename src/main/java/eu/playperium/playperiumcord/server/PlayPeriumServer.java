package eu.playperium.playperiumcord.server;

import net.md_5.bungee.api.ProxyServer;

public class PlayPeriumServer {

    public static String getPluginVersion() {
        return ProxyServer.getInstance().getPluginManager().getPlugin("PlayPeriumCord").getDescription().getVersion();
    }

    public static String getMinecraftVersion() {
        String version = "1.12.x";

        return version;
    }

    public static String getServerVersion() {
        return ProxyServer.getInstance().getVersion();
    }
}
