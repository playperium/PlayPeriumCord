package eu.playperium.playperiumcord;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;

public final class PlayPeriumCord extends Plugin implements Listener {

    @Override
    public void onEnable() {
        // Plugin startup logic
        setupEvents();
        registerCommands();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void setupEvents() {
        getProxy().getPluginManager().registerListener(this, this);
        getProxy().getPluginManager().registerListener(this, new eu.playperium.playperiumcord.listeners.ProxyListener(this));
        getProxy().getPluginManager().registerListener(this, new eu.playperium.playperiumcord.listeners.ConnectionListener(this));
        getProxy().getPluginManager().registerListener(this, new eu.playperium.playperiumcord.listeners.PlayerListener(this));
    }

    public void registerCommands() {
        getProxy().getPluginManager().registerCommand(this, new eu.playperium.playperiumcord.commands.CommandBungee("bungee"));
        getProxy().getPluginManager().registerCommand(this, new eu.playperium.playperiumcord.commands.CommandLobby("lobby"));
        getProxy().getPluginManager().registerCommand(this, new eu.playperium.playperiumcord.commands.CommandHub("hub"));
        getProxy().getPluginManager().registerCommand(this, new eu.playperium.playperiumcord.commands.CommandHelp("help"));
    }
}
