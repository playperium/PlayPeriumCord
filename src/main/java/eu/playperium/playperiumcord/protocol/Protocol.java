package eu.playperium.playperiumcord.protocol;

import java.util.Arrays;
import java.util.List;

public class Protocol {

    public static List<Integer> supportedVersions = Arrays.asList(
            MINECRAFT_1_12_2()
    );

// Protocol version numbers | http://wiki.vg/Protocol_version_numbers

    // Support 1.12.2
    public static Integer MINECRAFT_1_12_2() {
        Integer version = 340;
        return version;
    }

    // Support 1.10 - 1.10.2
    public static Integer MINECRAFT_1_10() {
        Integer version = 210;
        return version;
    }

    // Support 1.9.3 - 1.9.4
    public static Integer MINECRAFT_1_9() {
        Integer version = 110;
        return version;
    }

    // Support 1.8 - 1.8.9
    public static Integer MINECRAFT_1_8() {
        Integer version = 47;
        return version;
    }

    // Support 1.7.6 - 1.7.10
    public static Integer MINECRAFT_1_7() {
        Integer version = 5;
        return version;
    }
}
